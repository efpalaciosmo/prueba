package com.metnet.prueba;

import com.metnet.prueba.controllers.ProductsControllers;
import com.metnet.prueba.exceptions.ProductExceptions;
import com.metnet.prueba.persistence.entity.ProductInDto;
import com.metnet.prueba.persistence.repository.IProductRepository;
import com.metnet.prueba.services.ProductService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

public class ProductTest {
    @InjectMocks
    private ProductService productService;
    @Mock
    private IProductRepository productRepository;
    @Mock
    private ModelMapper modelMapper;

    @BeforeEach
    public void setup(){
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void getComicTest() throws ProductExceptions{
        given(productRepository.existsById(1L)).willReturn(false);
        Throwable throwable = assertThrows(ProductExceptions.class, ()-> productService.deleteProduct(1L));
    }
}
