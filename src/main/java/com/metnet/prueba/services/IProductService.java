package com.metnet.prueba.services;

import com.metnet.prueba.exceptions.ProductExceptions;
import com.metnet.prueba.persistence.entity.Product;
import com.metnet.prueba.persistence.entity.ProductInDto;
import com.metnet.prueba.persistence.entity.ProductOutDto;
import org.springframework.http.HttpStatus;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public interface IProductService {
    public void createProduct(ProductInDto productInDto);
    public ProductOutDto getProduct(Long id);
    public List<ProductOutDto> getAllProducts();
    public ProductOutDto updateProduct(ProductInDto productInDto);
    public void deleteProduct(Long id);
}