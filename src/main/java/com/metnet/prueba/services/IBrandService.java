package com.metnet.prueba.services;

import com.metnet.prueba.persistence.entity.BrandDto;

import java.util.List;

public interface IBrandService {
    public void createBrand(BrandDto brandDto);
    public List<BrandDto> getAllBrands();
}
