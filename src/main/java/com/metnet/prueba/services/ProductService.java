package com.metnet.prueba.services;

import com.metnet.prueba.exceptions.ProductExceptions;
import com.metnet.prueba.persistence.entity.Brand;
import com.metnet.prueba.persistence.entity.Product;
import com.metnet.prueba.persistence.entity.ProductInDto;
import com.metnet.prueba.persistence.entity.ProductOutDto;
import com.metnet.prueba.persistence.repository.IBrandRepository;
import com.metnet.prueba.persistence.repository.IProductRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ProductService implements IProductService {
    @Autowired
    private IProductRepository productRepository;
    @Autowired
    private IBrandRepository brandRepository;
    @Autowired
    private ModelMapper modelMapper;

    @Override
    public void createProduct(ProductInDto productInDto) {
        Product product = modelMapper.map(productInDto, Product.class);
        Optional< Brand> brand = brandRepository.findById(productInDto.getBrand_id());
        if(brand.isEmpty()){
            throw new ProductExceptions("Brand not found", HttpStatus.NOT_FOUND);
        }
        product.setBrand(brand.get());
        productRepository.save(product);
    }

    @Override
    public ProductOutDto getProduct(Long id) {
        Optional<Product> product = productRepository.findById(id);
        if(product.isEmpty()){
            throw new ProductExceptions("Product not found", HttpStatus.NOT_FOUND);
        }
        return modelMapper.map(product, ProductOutDto.class);
    }

    @Override
    public List<ProductOutDto> getAllProducts() {
        List<Product> productList = productRepository.findAll();
        return productList.stream()
                .map(product -> modelMapper.map(product, ProductOutDto.class))
                .collect(Collectors.toList());
    }

    @Override
    public ProductOutDto updateProduct(ProductInDto productInDto) {
        Long id = productInDto.getId();
        Optional<Product> productDb = productRepository.findById(id);
        if(productDb.isEmpty()){
            throw  new ProductExceptions("Product not found", HttpStatus.NOT_FOUND);
        }
        Product product = modelMapper.map(productInDto, Product.class);
        productRepository.save(product);
        return modelMapper.map(product, ProductOutDto.class);
    }

    @Override
    public void deleteProduct(Long id) {
        Optional<Product> productIn = productRepository.findById(id);
        if(productIn.isEmpty()){
            throw new ProductExceptions("Product not found", HttpStatus.NOT_FOUND);
        }
        productRepository.deleteById(id);
    }
}
