package com.metnet.prueba.services;

import com.metnet.prueba.exceptions.ProductExceptions;
import com.metnet.prueba.persistence.entity.*;
import com.metnet.prueba.persistence.repository.IBrandRepository;
import com.metnet.prueba.persistence.repository.IProductRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class BrandService implements IBrandService {
    @Autowired
    private IBrandRepository brandRepository;
    @Autowired
    private ModelMapper modelMapper;

    @Override
    public void createBrand(BrandDto brandDto) {
        Brand brand = modelMapper.map(brandDto, Brand.class);
        brandRepository.save(brand);
    }

    @Override
    public List<BrandDto> getAllBrands() {
        List<Brand> brandList = brandRepository.findAll();
        return brandList.stream()
                .map(brand -> modelMapper.map(brand, BrandDto.class))
                .collect(Collectors.toList());
    }
}
