package com.metnet.prueba.persistence.repository;

import com.metnet.prueba.persistence.entity.Brand;
import com.metnet.prueba.persistence.entity.BrandOutDto;
import com.metnet.prueba.persistence.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface IBrandRepository extends JpaRepository<Brand, Long> {
}
