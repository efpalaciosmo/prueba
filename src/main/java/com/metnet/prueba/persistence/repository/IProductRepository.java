package com.metnet.prueba.persistence.repository;

import com.metnet.prueba.persistence.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface IProductRepository extends JpaRepository<Product, Long> {
    Optional<Product> findProductByNameAndAndBrand(@Param("name") String name, @Param("brand") String brand);
}
