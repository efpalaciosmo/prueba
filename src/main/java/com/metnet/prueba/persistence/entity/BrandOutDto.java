package com.metnet.prueba.persistence.entity;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class BrandOutDto {
    private String brandName;
    private String brandLocation;
}
