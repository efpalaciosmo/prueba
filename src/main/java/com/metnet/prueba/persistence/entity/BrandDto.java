package com.metnet.prueba.persistence.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
public class BrandDto {
    private String brandName;
    private String brandLocation;
}
