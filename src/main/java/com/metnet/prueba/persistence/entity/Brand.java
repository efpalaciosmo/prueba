package com.metnet.prueba.persistence.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
public class Brand {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "brand_sequence")
    @SequenceGenerator(name = "brand_sequence", sequenceName = "brand_sequence", allocationSize = 1)
    @Column(name = "brand_id", updatable = false)
    private Long id;
    private String brandName;
    private String brandLocation;
    @OneToMany(
            cascade = {CascadeType.PERSIST, CascadeType.REMOVE}
    )
    @JoinColumn(name = "product_id", updatable = false)
    private List<Brand> brandList = new ArrayList<>();
}
