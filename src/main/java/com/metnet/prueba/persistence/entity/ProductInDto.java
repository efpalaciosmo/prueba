package com.metnet.prueba.persistence.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ProductInDto {
    private Long id;
    private String name;
    private Long brand_id;
    private String category;
    private Double price;
    private int quantity;
}
