package com.metnet.prueba.persistence.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ProductOutDto {
    private String name;
    private String category;
    private Brand brand;
    private int quantity;
    private Double price;
}
