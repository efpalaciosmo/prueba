package com.metnet.prueba.controllers;

import com.metnet.prueba.exceptions.ProductExceptions;
import com.metnet.prueba.persistence.entity.Brand;
import com.metnet.prueba.persistence.entity.BrandDto;
import com.metnet.prueba.persistence.entity.BrandOutDto;
import com.metnet.prueba.persistence.entity.ProductInDto;
import com.metnet.prueba.persistence.repository.IBrandRepository;
import com.metnet.prueba.services.BrandService;
import com.metnet.prueba.services.ProductService;
import com.metnet.prueba.utils.Util;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("v1")
public class BrandsControllers extends Util {
    @Autowired
    private BrandService brandService;
    @Autowired
    private ModelMapper modelMapper;

    @PostMapping("brand/create")
    public ResponseEntity<?> createBrand(@RequestBody BrandDto brandDto){
        brandService.createBrand(brandDto);
        return buildResponse("brand created");
    }
    @GetMapping("/brands")
    ResponseEntity<?> getAllBrands(){
        return buildResponse(brandService.getAllBrands());
    }
}