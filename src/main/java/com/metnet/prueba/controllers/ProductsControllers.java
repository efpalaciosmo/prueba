package com.metnet.prueba.controllers;

import com.metnet.prueba.exceptions.ProductExceptions;
import com.metnet.prueba.persistence.entity.ProductInDto;
import com.metnet.prueba.services.ProductService;
import com.metnet.prueba.utils.Util;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("v1")
public class ProductsControllers extends Util {
    @Autowired
    private ProductService productService;
    @Autowired
    private ModelMapper modelMapper;

    @PostMapping("products/create")
    public ResponseEntity<?> createProduct(@RequestBody ProductInDto productInDto){
        productService.createProduct(productInDto);
        return buildResponse("Product created");
    }

    @GetMapping("/products/{id}")
    public ResponseEntity<?> getProduct(@PathVariable("id") Long id) throws ProductExceptions {
        return buildResponse(productService.getProduct(id));
    }

    @GetMapping("/products")
    public ResponseEntity<?> getAllProducts(){
        return buildResponse(productService.getAllProducts());
    }

    @DeleteMapping("/products/{id}")
    public ResponseEntity<?> deleteProduct(@RequestParam Long id){
        productService.deleteProduct(id);
        return buildResponse("Product deleted");
    }

    @PatchMapping("/update-product")
    public ResponseEntity<?> updateProduct(@RequestBody ProductInDto productInDto) throws ProductExceptions {
        return buildResponse(productService.updateProduct(productInDto));
    }
}
