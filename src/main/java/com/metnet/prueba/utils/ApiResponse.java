package com.metnet.prueba.utils;

import com.fasterxml.jackson.annotation.JsonInclude;

public class ApiResponse<T> {
    public String status; // The status
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String message; // An informative string message
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String errorcode;
    public T data; // The mutable data object
    public ApiResponse (String status, T data, String message, String errorcode) {
        this.status = status;
        this.data = data;
        this.message = message;
        this.errorcode = errorcode;
    }
    public ApiResponse (String status, T data, String message) {
        this(status, data, message, null);
    }
    public ApiResponse (String status, T data) {
        this(status, data, null);
    }
    public ApiResponse () {
    }
}
