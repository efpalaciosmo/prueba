package com.metnet.prueba.utils;

import org.springframework.http.HttpStatus;

public abstract class Util {

    protected <T> JSONResponse<T> buildResponse (T data) {
        return new JSONResponse<>(JSONResponseStatus.SUCCESS.toString(), HttpStatus.OK, data);
    }
    protected <T> JSONResponse<T> buildErrorResponse (T error, HttpStatus httpStatus) {
        return new JSONResponse<>(JSONResponseStatus.ERROR.toString(), httpStatus, error);
    }
}
