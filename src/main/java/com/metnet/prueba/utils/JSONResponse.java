package com.metnet.prueba.utils;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class JSONResponse<T> extends ResponseEntity<ApiResponse<T>> {
    public JSONResponse (String status, HttpStatus httpStatus, T body) {
        this(status, httpStatus, body, null);
    }

    public JSONResponse (String status, HttpStatus httpStatus, T body, String message) {
        super(new ApiResponse<>(status, body, message), httpStatus);
    }
    public JSONResponse (String status, HttpStatus httpStatus, T body, String message, String errorcode) {
        super(new ApiResponse<>(status, body, message, errorcode), httpStatus);
    }
}
