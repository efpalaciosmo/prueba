package com.metnet.prueba.exceptions;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class ProductExceptions extends RuntimeException {
    private final String message;
    private final HttpStatus httpStatus;

    public ProductExceptions(String message, HttpStatus httpStatus){
        super(message);
        this.message = message;
        this.httpStatus = httpStatus;
    }
}
